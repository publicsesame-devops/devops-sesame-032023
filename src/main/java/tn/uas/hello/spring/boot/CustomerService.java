package tn.uas.hello.spring.boot;

import java.util.List;

public interface CustomerService {
	
	Customer create (Customer c);
	Customer update (Customer c);
	Customer readById (Long id);
	List<Customer> readAll();
	void delete (Customer c);
	void delete (Long id);


}
